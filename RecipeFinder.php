<?php

/**
 * Creates File Container base abstract object.
 */
abstract class FileContainer {

    /**
     * @var string Path of the file to load into the container.
     */
    protected $filePath;
    /**
     * @var array Items loaded.
     */
    protected $items;
    /**
     * @var array Indexed items
     */
    protected $indexedItems;

    function __construct($filePath) {
        $this->filePath = $filePath;
        $this->items = array();
        $this->indexedItems = array();
    }

    /**
     * Load the fridge items into the object from a file.
     *
     * @return array Items loaded.
     */
    abstract public function loadItems();

    /**
     * Add to item index.
     *
     * @return array Items loaded.
     */
    protected function addToIndex($key, $value) {
        if (!isset($this->indexedItems[$key])) {
            $this->indexedItems[$key] = array($value);
        } else {
            array_push($this->indexedItems[$key], $value);
        }
    }

    /**
     * Get item index.
     *
     * @return array Items loaded.
     */
    public function getIndex($key) {
        if (isset($this->indexedItems[$key])) {
            return $this->indexedItems[$key];
        }
    }

    /**
     * Load the fridge items into the object from a file.
     *
     * @return array Items loaded.
     */
    public function getItems() {
        return $this->items;
    }
}

/**
 * Creates Fridge.
 */
class Fridge extends FileContainer implements Iterator {

    private $position = 0;

    function __construct($filePath) {
        parent::__construct($filePath);
        $this->position = 0;
    }

    function rewind() {
        $this->position = 0;
    }

    function current() {
        return $this->items[$this->position];
    }

    function key() {
        return $this->position;
    }

    function next() {
        return ++$this->position;
    }

    function valid() {
        return isset($this->items[$this->position]);
    }

    public function loadItems() {

        $this->items = array();
        $fp = fopen($this->filePath, "r");
        if (!$fp) {
            throw new Exception("Could not open CSV file: {$this->filePath}");
        }

        // Parse rows of CSV. Store fridge item details, keyed by name for fast lookup.
        try {
            while (($row = fgetcsv($fp)) !== FALSE) {
                $obj = (object)array_combine(array('name', 'amount', 'unit', 'usedBy'), $row);
                array_push($this->items, $obj);
                $this->addToIndex($obj->name, $obj);
            }
        } catch (Exception $e) {
            throw new Exception("Invalid format in file: {$this->filePath}");
        }
        fclose($fp);

        return $this;
    }
}

/**
 * Creates Recipes.
 */
class Recipes extends FileContainer {

    public $ingredientIndex = array();

    /**
     * Load the fridge items into the object from a file.
     *
     * @param string $filePath
     */
    function __construct($filePath) {
        parent::__construct($filePath);
    }

    /**
     * Load the fridge items into the object from a file.
     *
     * @param object Fridge $fridge
     * @param object Recipe $fridge
     */
    public function loadItems() {

        $this->items = array();

        $contents = file_get_contents($this->filePath);
        if ($contents) {
            $this->items = json_decode($contents);
            if (is_null($this->items)) {
                throw new Exception("Invalid format JSON file: {$this->filePath}");
            }
            // Create indexes on recipe ingredients
            foreach($this->items as $recipe) {
                foreach ($recipe->ingredients as $ingredient) {
                    $this->addToIndex($ingredient->item, $recipe);
                }
            }
        }
        return $this;
    }
}

/**
 * Creates Recipe Finder.
 */
class RecipeFinder {

    const NO_RECIPES_FOUND = 'Order Takeout';

    /**
     * @var object Fridge Injected Fridge object.
     */
    private $fridge;
    /**
     * @var object Recipes Injected Recipes object.
     */
    private $recipes;

    /**
     * Constructor injecting fridge and recipe objects.
     *
     * @param object Fridge $fridge
     * @param object Recipe $fridge
     */
    function __construct($fridge, $recipes) {
        $this->fridge = $fridge;
        $this->recipes = $recipes;
    }

    /**
     * Finds a recipe.
     *
     * @return string Recipe name of best matching recipe.
     */
    public function find() {

        /**
         * Checks if the fridge food item is usable (ie. not expired)
         *
         * @param object $recipe Recipe
         * @param object $foodDetails Fridge item details
         * @return string|null Returns usedByDate in ISO format when usable.
         */
        function isFoodUsable($food) {
            $splitDate = explode('/', $food->usedBy);
            $usedByDate = sprintf('%04d/%02d/%02d', $splitDate[2], $splitDate[1], $splitDate[0]);
            return $usedByDate > date('Y/m/d') ? $usedByDate : null;
        }

        /**
         * Checks if the fridge food item is enough for the recipe.
         *
         * @param object $ingredient Ingredient
         * @param object $food Fridge item details
         * @return boolean true when ok.
         */
        function isEnoughFood($ingredient, $food) {
            return $food->unit === $ingredient->unit &&
                    $food->amount >= $ingredient->amount;
        }

        // Go through fridge items, seeing if they can be used in any recipes

        $matchingRecipe = null;
        $closestUsedByDate = null;

        foreach ($this->fridge as $food) {

            // Ignore fridge food item if expired.
            // Check if food found in indexed recipes.

            if (isFoodUsable($food) && ($indexedRecipes = $this->recipes->getIndex($food->name))) {

                // Food item is in at least 1 recipe. Scan through those recipes.
                foreach ($indexedRecipes as $recipe) {

                    // Now check if other recipe ingredients are in fridge.
                    $allIngredientsMatch = true;
                    $minUsedByDate = null;
                    foreach ($recipe->ingredients as $ingredient) {
                        //var_dump($ingredient);
                        // Check if ingredient in fridge.
                        $indexedfridgeItems = $this->fridge->getIndex($ingredient->item);
                        if (!$indexedfridgeItems) {
                            // Not in fridge at all.
                            $allIngredientsMatch = false;
                            break;
                        }
                        // Check if the fridge food is usable and enough.
                        $fridgeItemsMatched = false;
                        foreach ($indexedfridgeItems as $fridgeItem) {

                            if (($usedByDate = isFoodUsable($fridgeItem)) && isEnoughFood($ingredient, $fridgeItem)) {

                                $fridgeItemsMatched = true;
                                if (!$minUsedByDate || $usedByDate < $minUsedByDate) {
                                    // Store oldest ingredient used by date.
                                    $minUsedByDate = $usedByDate;
                                }
                                break;
                            }
                        }
                        if (!$fridgeItemsMatched) {
                            $allIngredientsMatch = false;
                            break;
                        }
                    }
                    if ($allIngredientsMatch) {
                        // Suitable recipe found.
                        if (!$matchingRecipe || $minUsedByDate < $closestUsedByDate) {
                            $matchingRecipe = $recipe;
                        }
                    }
                }
            }
        }
        return $matchingRecipe ? $matchingRecipe->name : self::NO_RECIPES_FOUND;
    }
}
