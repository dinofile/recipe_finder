<?php

error_reporting(E_ERROR);

require('RecipeFinder.php');

define('USAGE_MSG', 'Usage args: <fridge_csv_filename> <recipes_json_filename>');

function getCmdLineArgs($argc, $argv) {
    if ($argc < 3) {
        throw new Exception(USAGE_MSG);
    }
    return array($argv[1], $argv[2]);
}

// Main handling

try {
    // Get filenames from command args
    list($fridgeFilename, $recipeFilename) = getCmdLineArgs($argc, $argv);

    // Create fridge and load items from file.
    $fridge = new Fridge($fridgeFilename);
    $fridge->loadItems();

    $recipes = new Recipes($recipeFilename);
    $recipes->loadItems();

    // Create recipe finder object.
    $recipeFinder = new RecipeFinder($fridge, $recipes);

    // Run recipe finder
    print($recipeFinder->find());

} catch (Exception $e) {
    // Any caught errors, simply display their message text.
    $stderr = fopen('php://stderr', 'w');
    fwrite($stderr, $e->getMessage() ."\n");
}
