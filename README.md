### To run ###

php index.php fridge.csv recipes.json

### Implementation ###

An attempt was made to provide a solution that was fast.

When doing a recipe find it will follow these steps:
- load fridge csv and index by item name
- load recipes json and index recipes name by ingredient names
- search through fridge, ignoring expired items
-- lookup recipes from indexed recipes by ingredient name
-- for each of those recipes, look back in fridge and check each ingredient

I assumed the fridge would always be a relatively small number of items, whereas the recipes could grow very large.
Seemed quicker index and lookup than just sequentially scan recipes.

### Testing ###

It was not built using proper TDD methodologies, as I had limited time to do this test.
Apologies, I also did not have the time to add test cases.
Naturally they would be added in a real environment.